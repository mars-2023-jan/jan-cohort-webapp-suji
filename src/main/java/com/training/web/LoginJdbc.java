package com.training.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import net.javaguides.login.bean.LoginBean;

public class LoginJdbc {

	public static boolean validate(String username, String password) throws ClassNotFoundException {
		boolean valid = false;

		Class.forName("com.mysql.jdbc.Driver");

		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mars_jan", "root",
				"Friend123");

				// Create a statement using connection object
				PreparedStatement pstmt = connection
						.prepareStatement("select * from user_table where username = ? and password = ? ")) {
			pstmt.setString(1, username);
			pstmt.setString(2, password);

			System.out.println(pstmt);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				valid = true;
			}

		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
	}

}
