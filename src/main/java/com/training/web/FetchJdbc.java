package com.training.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import net.javaguides.login.bean.LoginBean;

public class FetchJdbc {

	public static ArrayList<UserTable> getData() throws ClassNotFoundException {
		ArrayList<UserTable> userTableList = new ArrayList<UserTable>();

		Class.forName("com.mysql.jdbc.Driver");

		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mars_jan", "root",
				"Friend123"); PreparedStatement pstmt = connection.prepareStatement("select * from user_table")) {
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String userName = rs.getString(2);
				String pass = rs.getString(3);
				UserTable userTable = new UserTable(id, userName, pass);
				userTableList.add(userTable);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userTableList;
	}

}
