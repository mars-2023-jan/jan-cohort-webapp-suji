<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import ="java.util.List" %>
   
    <%@page import = "com.training.web.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<h1>Welcome ${usrname}!!</h1>
<h1>User Table</h1>

<% List<UserTable> userList = (List<UserTable>)request.getAttribute("userList"); %>

<table border ="1">

	<tr>
	<th>Id</th>
	<th>UserName</th>
	<th>Password</th>
</tr>

<% 
	for (UserTable userTable: userList){ %>
	<tr>
		<td><%=userTable.getId() %></td>
		<td><%=userTable.getUserName() %></td>
		<td><%=userTable.getPassword() %></td>
		</tr>
<%} %>
</table>	
</body>
</html>