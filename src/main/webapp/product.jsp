<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <%@page import ="java.util.List" %>
   
    <%@page import = "com.training.web.model.Product" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<h1>Welcome ${usrname}!!</h1>

<% List<Product> prdList = (List<Product>)request.getAttribute("prdList"); %>

<table border ="1">

	<tr>
	<th>ProdId</th>
	<th>ProdName</th>
	<th>ProdDesc</th>
	<th>Price</th>
</tr>

<c:forEach ="${prdList} var = "product">

<tr>
	<td>${product.getProdId()}</td>
	<td>${product.getProdName()}</td>
	<td>${product.getProdDesc()}</td>
	<td>${product.getPrice()}</td>
	
</tr>
</c:forEach>
</table>	

</body>
</html>
